﻿namespace Genius.DotNet.PDK.Payloads;

internal class MessagePayload
{
    public string Title { get; set; }
    public string Content { get; set; }
}