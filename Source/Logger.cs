using System.Runtime.InteropServices;
using Genius.DotNet.PDK.Payloads;

namespace Genius.DotNet.PDK;

public static class Logger
{
    [DllImport("env", EntryPoint = "genius_log")]
    private static extern ulong Log(ulong payload);

    internal static void Log(LogLevel level, string message)
    {
        var payload = new LogPayload
        {
            Level = level,
            Message = message
        };

        Log(Functions.AllocatePayload(payload));
    }

    public static void Info(string message)
    {
        Log(LogLevel.Info, message);
    }

    public static void Debug(string message)
    {
        Log(LogLevel.Debug, message);
    }

    public static void Warning(string message)
    {
        Log(LogLevel.Warning, message);
    }

    public static void Error(string message)
    {
        Log(LogLevel.Error, message);
    }
}