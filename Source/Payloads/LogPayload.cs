﻿namespace Genius.DotNet.PDK.Payloads;

public class LogPayload
{
    public LogLevel Level { get; set; }
    public string Message { get; set; }
}