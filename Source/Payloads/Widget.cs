namespace Genius.DotNet.PDK.Payloads;

public class Widget
{
    public string Name { get; set; }
    public string Template { get; set; }
    public string EditTemplate { get; set; }

    public string Icon { get; set; }
}