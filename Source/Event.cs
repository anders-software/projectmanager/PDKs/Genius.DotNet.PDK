﻿using System.Text.Json.Serialization;

namespace Genius.DotNet.PDK;

public class Event(string name, object arg)
{
    [JsonPropertyName("name")] public string Name { get; } = name;

    [JsonPropertyName("arg")] public object Arg { get; } = arg;

    public override bool Equals(object? obj)
    {
        if (obj is Event e) return e.Name.Equals(Name);

        return false;
    }

    public static bool operator ==(Event e1, Event e2)
    {
        if (ReferenceEquals(e1, null) && ReferenceEquals(e2, null)) return true;

        if (ReferenceEquals(e1, null) || ReferenceEquals(e2, null)) return false;

        return e1.Equals(e2);
    }

    public static bool operator !=(Event e1, Event e2)
    {
        return !(e1 == e2);
    }

    public static Event operator +(Event e, Action<object> callback)
    {
        EventSystem.Subscribe(e, callback);

        return e;
    }

    public static Event operator +(Event e, Action callback)
    {
        EventSystem.Subscribe(e, _ => callback());

        return e;
    }

    public override string ToString()
    {
        return Name;
    }
}