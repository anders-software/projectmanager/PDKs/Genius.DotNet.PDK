﻿namespace Genius.DotNet.PDK;

public enum LogLevel
{
    Error,
    Warning,
    Info,
    Debug
}