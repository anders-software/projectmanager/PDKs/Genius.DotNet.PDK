﻿namespace Genius.DotNet.PDK.Payloads;

internal class GetResourcePayload
{
    public string Name { get; set; }
}