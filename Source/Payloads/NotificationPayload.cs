﻿namespace Genius.DotNet.PDK.Payloads;

internal class NotificationPayload
{
    public string Content { get; set; }
}