﻿using System.Runtime.InteropServices;
using Genius.DotNet.PDK.Payloads;

namespace Genius.DotNet.PDK;

public static class UI
{
    [DllImport("env", EntryPoint = "show_messagebox")]
    private static extern ulong ShowMessageBox(ulong payload);

    [DllImport("env", EntryPoint = "show_notification")]
    private static extern ulong ShowNotification(ulong payload);

    [DllImport("env", EntryPoint = "create_page")]
    private static extern ulong CreatePage(ulong payload);

    public static void ShowMessageBox(string title, string content)
    {
        var payload = new MessagePayload {Content = content, Title = title};

        ShowMessageBox(Functions.AllocatePayload(payload));
    }

    public static void CreatePage(string title, string path, string iconName, string contentXaml)
    {
        var payload =
            new CreatePagePayload {Title = title, Path = path, IconName = iconName, ContentXaml = contentXaml};

        CreatePage(Functions.AllocatePayload(payload));
    }

    public static void ShowNotification(string content)
    {
        var payload = new NotificationPayload {Content = content};

        ShowMessageBox(Functions.AllocatePayload(payload));
    }
}