﻿namespace Genius.DotNet.PDK;

public class PluginMetadata
{
    public PluginMetadata()
    {
    }

    public PluginMetadata(string name, string version)
    {
        Name = name;
        Version = version;
    }

    public string Name { get; set; }
    public string Version { get; set; }
}