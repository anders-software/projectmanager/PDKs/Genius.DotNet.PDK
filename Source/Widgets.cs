using System.Runtime.InteropServices;
using Genius.DotNet.PDK.Payloads;

namespace Genius.DotNet.PDK;

public class Widgets
{
    [DllImport("env", EntryPoint = "register_widget")]
    private static extern ulong RegisterWidget(ulong payload);

    public static void Register(string name, string icon, string template, string editTemplate)
    {
        var widget = new Widget
        {
            Name = name,
            Icon = icon,
            Template = template,
            EditTemplate = editTemplate
        };

        var payload = Functions.AllocatePayload(widget);

        RegisterWidget(payload);
    }
}