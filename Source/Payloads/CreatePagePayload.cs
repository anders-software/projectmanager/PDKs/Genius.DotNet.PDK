﻿namespace Genius.DotNet.PDK.Payloads;

internal class CreatePagePayload
{
    public string Title { get; set; }
    public string Path { get; set; }
    public string IconName { get; set; }
    public string ContentXaml { get; set; }
}