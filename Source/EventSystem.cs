using System.Runtime.InteropServices;
using System.Text.Json;
using Extism;

namespace Genius.DotNet.PDK;

public static class EventSystem
{
    private static readonly Dictionary<string, Delegate?> Subscriptions = new();

    [DllImport("env", EntryPoint = "dispatch_event")]
    private static extern ulong DispatchEvent(ulong payload);

    [UnmanagedCallersOnly(EntryPoint = "on_event")]
    public static void OnEvent()
    {
        var json = Pdk.GetInputString();

        var e = JsonSerializer.Deserialize(json, SourceGenerationContext.Default.Event)!;

        if (Subscriptions.TryGetValue(e.Name, out var subscription)) subscription!.DynamicInvoke(e.Arg);
    }

    public static void Invoke(string name, object? arg = null)
    {
        DispatchEvent(Functions.AllocatePayload(new Event(name, arg)));
    }

    public static void Invoke(Event @event, object? arg = null)
    {
        DispatchEvent(Functions.AllocatePayload(new Event(@event.Name, arg)));
    }

    public static Event Register(string name)
    {
        var e = new Event(name, null);
        Subscriptions.TryAdd(name, (object _) => { });

        return e;
    }

    public static void Subscribe(Event e, Action<object> callback)
    {
        if (!Subscriptions.TryAdd(e.Name, callback))
            Subscriptions[e.Name] = Delegate.Combine(Subscriptions[e.Name], callback);
    }
}