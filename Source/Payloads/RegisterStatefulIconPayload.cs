﻿namespace Genius.DotNet.PDK.Payloads;

internal class RegisterStatefulIconPayload
{
    public string Name { get; set; }
    public string NormalPath { get; set; }
    public string ActivePath { get; set; }
}