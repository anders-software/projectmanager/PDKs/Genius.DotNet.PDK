﻿using System.Text.Json;
using System.Text.Json.Serialization;
using Extism;

namespace Genius.DotNet.PDK;

[JsonSerializable(typeof(Event))]
public partial class SourceGenerationContext : JsonSerializerContext
{
}

public static class Functions
{
    internal static ulong AllocatePayload(object payload)
    {
        return Pdk.Allocate(SerializeObject(payload)).Offset;
    }

    internal static T GetReturnValue<T>(ulong ptr)
    {
        var memory = MemoryBlock.Find(ptr);
        var json = memory.ReadString();

        return JsonSerializer.Deserialize<T>(json)!;
    }

    public static void Init()
    {
    }

    public static string SerializeObject(object obj)
    {
        return JsonSerializer.Serialize(obj);
    }

    public static void SetOutput(object obj)
    {
        Pdk.SetOutput(SerializeObject(obj));
    }

    public static void Main()
    {
    }
}