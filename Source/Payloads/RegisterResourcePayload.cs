﻿namespace Genius.DotNet.PDK.Payloads;

internal class RegisterResourcePayload
{
    public string Name { get; set; }
    public object Value { get; set; }
}