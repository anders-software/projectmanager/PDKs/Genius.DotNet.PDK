﻿using System.Runtime.InteropServices;
using Genius.DotNet.PDK.Payloads;

namespace Genius.DotNet.PDK;

public static class ResourceLoader
{
    [DllImport("env", EntryPoint = "register_icon")]
    private static extern ulong RegisterIcon(ulong payload);

    [DllImport("env", EntryPoint = "register_resource")]
    private static extern ulong RegisterResource(ulong payload);

    [DllImport("env", EntryPoint = "get_resource")]
    private static extern ulong GetResource(ulong payload);

    public static void RegisterIcon(string name, string path)
    {
        var payload = new RegisterResourcePayload {Name = name, Value = path};

        RegisterIcon(Functions.AllocatePayload(payload));
    }

    public static void RegisterStatefulIcon(string name, string normalPath, string activePath)
    {
        var payload = new RegisterStatefulIconPayload {Name = name, NormalPath = normalPath, ActivePath = activePath};

        RegisterIcon(Functions.AllocatePayload(payload));
    }

    public static void RegisterResource(string name, object value)
    {
        var payload = new RegisterResourcePayload {Name = name, Value = value};

        RegisterResource(Functions.AllocatePayload(payload));
    }

    public static T GetResource<T>(string name)
    {
        var payload = new GetResourcePayload {Name = name};

        var ptr = GetResource(Functions.AllocatePayload(payload));

        return Functions.GetReturnValue<T>(ptr);
    }
}